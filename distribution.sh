#!/usr/bin/bash

if [[ $# -ge 1 ]]
then
	DICT=$1
else
	DICT=alphanum
fi

CMD=dist/build/paphragen/paphragen

REPEAT=100000
LEN=10

PASS_FILE=$(mktemp)
FREQ_FILE=$(mktemp)

$CMD generate --length $LEN --count $REPEAT $DICT >> "$PASS_FILE"

echo "frequency, char" > "$FREQ_FILE"

fold -w1 "$PASS_FILE" | sort | uniq -c | sed 's:^ *\([0-9]*\):\1,:' >> "$FREQ_FILE"
./distribution.r "$FREQ_FILE"

rm "$PASS_FILE" "$FREQ_FILE"
