#!/usr/bin/Rscript

argv <- commandArgs(trailingOnly = TRUE)
d <- read.csv(argv[1], stringsAsFactors=TRUE)
summary(d$frequency)

n = length(d$char)

pdf("Distribution.pdf", width=8, height=3)
plot(x = d$char, y = d$frequency, ylim=c(0, max(d$frequency)))

print(d)

chisq.test(d$frequency)
