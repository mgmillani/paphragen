## Compiling

Install `ghc` and `cabal`. Then run

    cabal update
    cabal sandbox init
    cabal install --dependencies-only
    cabal build

This will generate the binary `paphragen` in `dist/build/paphragen/`.
# Synopsis

    paphragen build [OPTIONS...] FILE
    paphragen generate [OPTIONS...] FILE

# Description

A passphrase is a password made with words instead of just letters and digits.
The advantage over passwords is that they are easier to remember.
The disadvantage is that they are quite long and people tend to overestimate their security.

Most tools for password generation that we know just generate a random sequence of characters and cannot properly estimate the strength of a passphrase.

`paphragen` is capable not only of generating passphrases from a given word list, it can also build such word lists given sufficient text input (e.g. books, news articles...). It also properly computes the strength of the generated password. A stream of random bytes can be used in order to achieve cryptographic-quality randomness.

# Features

  - Generate passphrases with the desired security (that is, entropy).
  - Any wordlist can be used.
  - Build wordlists from real text. The generated wordlist is always prefix-free.
  - Support for hardware-generated truly-random bytestrings.
  - Support for hash-based passphrase generation. 

# Options for generating passphrases

  **`-e, --entropy N`**

  Sets the minimum desired entropy (default: 100 bits).
  
  **`-l, --length N`**
  
  Specify the number of words to use (overrides entropy).
  
  **`    --info`**

  Provides entropy information about the dictionary used.

# Options for building wordlists

  **`-c, --count N`**

  Generates N passwords, printing one per line.

  **`-l, --length N`**

  Takes N words into the dictionary. Too common (e.g., stop words) and too rare words are taken last.

  **`-o, --output DICTIONARY`**

  Writes output to DICTIONARY instead of stdout.

# Directories

Dictionaries are searched for in the following paths:

1. $XDG_CONFIG_HOME/paphragen
2. $HOME/.paphragen
3. /usr/share/paphragen

# Examples

To build a dictionary with 7776 words (for 5 dice):

    paphragen build -l 7776 -o dictionary book1.txt book2.txt

To generate a passphrase with 80 bits of entropy:

    paphragen generate --entropy 80 dictionary < /dev/random

To generate a passphrase with 4 words:

    paphragen generate --length 4 dictionary < /dev/random

# Bugs

No known bugs.

# Author

Marcelo Garlet Millani (marcelogmillani@gmail.com)
