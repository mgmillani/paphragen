--  Copyright 2016 Marcelo Garlet Millani
--  This file is part of paphragen.

--  paphragen is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  paphragen is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with paphragen.  If not, see <http://www.gnu.org/licenses/>.


module Main where

import Data.List
import qualified Data.ByteString.Lazy as BS
import System.Environment
import System.Directory
import System.IO

import Paphragen.Build
import Paphragen.Generate

import qualified Debug.Trace as D

appname = "paphragen"
appversion = "0.2.0.0"

data Command = Help | Build | Generate deriving (Eq, Show, Ord)
data Action =
  Action
  { cmd         :: Command
  , outFile     :: FilePath
  , passLength  :: Int
  , passEntropy :: Int
  , passCount   :: Int
  , dictLength  :: Int
  , inputFiles  :: [FilePath]
  , infoFun     :: String -> IO ()
  }

defaultAction =
  Action
  { cmd         = Help
  , outFile     = ""
  , passLength  = 0
  , passEntropy = 100
  , passCount   = 1
  , dictLength  = 6^5
  , inputFiles  = []
  , infoFun     = quiet
  }

quiet :: String -> IO ()
quiet _ = return ()

-- | Searches for the file in the following directories:
--   1. $XDG_CONFIG_HOME/paphragen
--   2. $HOME/.paphragen
--   3. /usr/share/paphragen
-- | If the file is not found in any of these directories, the same path is returned.
findFileConfig fl = do
  xdg <- getXdgDirectory XdgConfig "paphragen"
  app <- getAppUserDataDirectory "paphragen"
  findFileDirs fl [xdg, app, "/usr/share/paphragen"]

findFileDirs fl dirs = do
  let dirsFl = map ( ++ ('/':fl)) dirs
  dirsE <- mapM doesFileExist $ dirsFl
  let results = dropWhile (\(fl,e) -> not e) $ zip dirsFl dirsE
  return $ if null results then fl else fst $ head results

-- | Loads a dictionary from a file.
loadDictionary fl = do
  fl' <- findFileConfig fl
  text <- readFile fl'
  return $ filter (not . null) $ lines text

-- | Prints help text.
help = do
  mapM_ putStrLn [
      appname ++ " " ++ appversion
    , "A passphrase generator."
    ,  "usage:"
    , "To build a dictionary based on the words of input files:"
    , "\t" ++ appname ++ " build [OPTIONS...] <TEXT_FILE...>"
    , "\t  where OPTIONS are:"
    , "\t\t-l, --length N              takes N words into the dictionary."
    , "\t\t-o, --output DICTIONARY     writes output to DICTIONARY instead of stdout."
    , "\nTo generate a password using an existing dictionary:"
    , "\t" ++ appname ++ " generate [OPTIONS...] <DICTIONARY...>"
    , "\t  where OPTIONS are:"
    , "\t\t-c, --count N        number of passwords to generate."
    , "\t\t-e, --entropy N      sets the minimum desired entropy (default: " ++ show (passEntropy defaultAction) ++ " bits)."
    , "\t\t-l, --length N       number of words to use (entropy is used by default)."
    , "\t\t    --info           prints entropy information together with password."
    , "\tIn this case, a random sequence of bytes should be provided through stdin."
    , "\tOn GNU/Linux, /dev/random is a good choice."
    ]

parseArgs action args
  | cmd action == Build = case args of
    "-o"      :file:rs -> parseArgs action{outFile = file} rs
    "--output":file:rs -> parseArgs action{outFile = file} rs
    "-l"      :n:rs -> parseArgs action{dictLength = read n} rs
    "--length":n:rs -> parseArgs action{dictLength = read n} rs
    rs -> action{inputFiles = rs}
  | cmd action == Generate = case args of
    "-c"     :count:rs -> parseArgs action{passCount = read count} rs
    "--count":count:rs -> parseArgs action{passCount = read count} rs
    "-l"      :len:rs -> parseArgs action{passLength = read len} rs
    "--length":len:rs -> parseArgs action{passLength = read len} rs
    "-e"       :ent:rs  -> parseArgs action{passEntropy = read ent} rs
    "--entropy":ent:rs  -> parseArgs action{passEntropy = read ent} rs
    "--info":rs -> parseArgs action{infoFun = putStrLn} rs
    rs -> action{inputFiles = rs}
  | cmd action == Help = case args of
    "build":rs -> parseArgs action{cmd = Build} rs
    "generate":rs -> parseArgs action{cmd = Generate} rs
    rs -> parseOptions action rs
parseOptions action args = case args of
  "-o"      :file:rs -> parseArgs action{outFile = file}         rs
  "--output":file:rs -> parseArgs action{outFile = file}         rs
  "-c"     :count:rs -> parseArgs action{passCount = read count} rs
  "--count":count:rs -> parseArgs action{passCount = read count} rs
  "-l"      :len:rs  -> parseArgs action{passLength  = read len} rs
  "--length":len:rs  -> parseArgs action{passLength  = read len} rs
  "-e"       :ent:rs -> parseArgs action{passEntropy = read ent} rs
  "--entropy":ent:rs -> parseArgs action{passEntropy = read ent} rs
  "--info":rs        -> parseArgs action{infoFun = putStrLn}     rs
  _ -> action{cmd = Help}

floatShow f =
  let f'  = f + 0.05
      str = show f'
      (int, dec) = span (/= '.') str
  in int ++ take 2 dec

execute action
  | cmd action == Build    = execBuild action
  | cmd action == Generate = execGenerate action
  | cmd action == Help     = help
execBuild action
  | null $ outFile action = do
    buildDictionary (dictLength action) (inputFiles action) stdout
  | otherwise = do
    withFile (outFile action) WriteMode (buildDictionary (dictLength action) (inputFiles action))
    putStrLn $ "Common words written to " ++ (outFile action)
execGenerate action
  | null $ inputFiles action = execute action{cmd = Help}
  | otherwise = do
    dict <- (mapM loadDictionary $ inputFiles action) >>= (return . concat)
    let n = length dict
    (infoFun action) $ "Dictionary has " ++ show n ++ " words."
    let epw = logBase 2 (fromIntegral n)
    (infoFun action) $ "This gives an entropy of " ++ floatShow epw ++ " bits per word."
    let k = if passLength action == 0 then ceiling $ (fromIntegral $ passEntropy action) / epw else passLength action
    (infoFun action) $ "Generating a password with " ++ show k ++ " words (" ++ floatShow ((fromIntegral k) * epw) ++ " bits of entropy)."
    randomness <- BS.getContents
    let passwords = generatePasswords (passLength action) (passCount action) dict n randomness
    mapM_ putStrLn passwords   

main :: IO ()
main = do
  args <- getArgs
  let action = parseArgs defaultAction args
  execute action
