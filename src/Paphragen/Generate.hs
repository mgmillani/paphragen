module Paphragen.Generate where

import qualified Data.ByteString.Lazy as BS
import qualified Data.Word as W
import qualified Data.Bits as B
import Data.List

-- | Take elements from a list with the given indices. The first component of the tuple is the index on the list. The second element is preserved.
-- | This is used to draw many words from a list without having to iterate through it multiple times.
takeIndices is ds =
  takeIndices' 0 is ds
  where
    takeIndices' k [] ds = []
    takeIndices' k ((x,i):is) ds = (head rs, i) : takeIndices' x is rs
      where
        rs = drop (x-k) ds

-- | Generates indices for a set of n elements using r as a source of randomness
randomIndices n rs
  | x < n     = x : randomIndices n (BS.drop bytes rs)
  | otherwise = randomIndices n (BS.drop bytes rs)
  where
    entropy = ceiling $ logBase 2 (fromIntegral n)
    bytes = ceiling $ logBase 256 (fromIntegral n)
    mask = 2^entropy - 1
    x = mask B..&. (fromIntegral $ BS.foldl accum (0 :: W.Word) (BS.take bytes rs))
    accum b x = (B.shift b 8) B..|. (fromIntegral x)

generatePasswords len count dictionary dictionaryLength randomness =
  let indices = zip (take (len * count) $ randomIndices dictionaryLength randomness) [0..]
      sInd = sortBy (\x y -> (fst x) `compare` (fst y)) indices
      pickedWords = map fst $ sortBy (\x y -> (snd x) `compare` (snd y)) $ takeIndices sInd dictionary
      passwords [] = []
      passwords ws = (concat $ take len ws) : passwords (drop len ws)
  in passwords pickedWords
