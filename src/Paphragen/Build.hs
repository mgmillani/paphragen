module Paphragen.Build (buildDictionary) where

import qualified Data.Map.Strict as Map
import Data.Char
import Data.List
import Data.Maybe

import System.IO

import qualified Debug.Trace as D

data Trie = Trie (Map.Map Char Trie) deriving (Show)
data Term = Term Float String deriving (Eq, Show, Ord)

-- | Splits a text into words.
wordSplit [] = []
wordSplit text =
  (map toLower word) : (wordSplit rest)
  where
    (word, text') = span isLetter text
    rest = dropWhile (not . isLetter) text'

-- | Writes a dictionary to a file. The file will have one word per line in ascending alphabetical order.
writeDictionary dict handle = do
  mapM_ (hPutStrLn handle) $ dict

-- | Counts how often each word occurs on the corpus.
wordScore files = wordScore' Map.empty Map.empty 0 files
wordScore' termCount documentCount n [] = do
  let tc' = Map.map fromIntegral termCount
      dc' = Map.map fromIntegral documentCount
  return $ Map.unionWith
              (\tf df -> tf * (log $ n / df) :: Float)
              tc' dc'
wordScore' termCount documentCount n (f:fs) = do
  text <- readFile f
  let tc  = foldl (\d w -> Map.insertWith (+) w 1 d) Map.empty $ wordSplit text
      dc' = foldl (\d w -> Map.insertWith (+) w 1 d) documentCount $ Map.keys tc
      tc' = Map.unionWith (+) termCount tc 
  wordScore' tc' dc' (n+1) fs

buildTrie words = buildTrie' Map.empty words
buildTrie' trie words =
  foldl insertTrie trie words
  where
    insertTrie dict [] = Map.insert '\0' (Trie Map.empty) dict
    insertTrie dict (w:ws) =
      Map.alter (\t -> case t of
        Nothing -> Just $ Trie $ insertTrie Map.empty ws
        Just (Trie t') -> Just $ Trie $ insertTrie t' ws) w dict 

prefixFree dict =
  let trie = buildTrie $ Map.keys dict
      independentSet trie
        | Map.size trie == 1 && Map.member '\0' trie = (1, [[]])
        | otherwise = 
          let isVertex = Map.member '\0' trie
              children' = map (\(k, (Trie m)) ->
                               let (n, ws) = independentSet m
                               in (n, map (k:) ws)) 
                              $ Map.toAscList trie
              nChildren = foldl (+) 0 $ map fst children'
              children = if nChildren == 2 && isVertex then (1, []) else (nChildren, concatMap snd children')
          in children
  in Map.intersection dict $ Map.fromList $ map (\x -> (x, 1)) $ snd $ independentSet trie

wordFrequency files = do
  text <- mapM readFile files
  let count = foldl (\d w -> Map.insertWith (+) w 1 d) Map.empty $ concatMap wordSplit text
      count' = prefixFree count
      nWords = sum $ Map.elems count
      freq = sort $ map (\(t, c) -> Term (fromIntegral c / fromIntegral nWords) t) $ Map.toAscList count'
  return $ freq

-- | Builds a dictionary form a list of files, writing the output to the given handle.
buildDictionary size files out = do
  wordFreq <- wordFrequency files
  --let common = Map.filterWithKey (\w s -> length w > 2 && s > 1 && s < 20) $ ws
  let n = length wordFreq
      (Term median mw) = wordFreq !! (n `div` 2)
      common = take size $ sort $ map (\(Term f w) -> Term (abs $ f - median) w) wordFreq
      common' = sort $ map (\(Term f w) -> w) common
  writeDictionary common' out
