# Revision history for paphragen

## 1.1.0.0 -- 2018.08.15
* Fixed a bug where the number of bytes used to generate each word was being computed wrongly.
* Added the ability to generate multiple passwords in a single call.
## 1.0.0.0 -- 2018.07.26
* Construction of prefix-free dictionaries.
* By default, output only password. Entropy info is given only if specified.

## 0.2.1.1 -- 2018.06.15
* Added documentation.

## 0.2.1.0  -- 2018.06.14
* Added quiet mode.
* Search for dictionaries in configuration folders.

## 0.2.0.0  -- 2016.12.30

* First version.
    - A dictionary can be built based on text files.
    - Passphrases with desired entropy are generated.
    - Uses a stream of random bytes for generation.
* TODO
    - Check performance of IO operations.
    - Generate a non-ambiguous dictionary, that is, guarantee that there is only one way of generating each passphrase. For example, if `in`, `put` and `input` are in the dictionary, the passphrase `input` is ambiguous. In this case, either `in` or `put` should be removed.
