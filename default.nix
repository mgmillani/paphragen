{ mkDerivation, base, bytestring, containers, directory, lib }:
mkDerivation {
  pname = "paphragen";
  version = "1.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base bytestring containers directory
  ];
  description = "A passphrase generator";
  license = lib.licenses.gpl3Only;
}
